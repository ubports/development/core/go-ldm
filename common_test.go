/*
 * Copyright 2014 Canonical Ltd.
 * Copyright 2024 Guido Berhoerster <guido+ubports@berhoerster.name>
 *
 * Authors:
 * Manuel de la Pena: manuel.delapena@canonical.com
 *
 * This file is part of lomiri-download-manager.
 *
 * lomiri-download-manager is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * lomiri-download-manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ldm

import (
	"errors"
	"strings"
	"testing"

	"github.com/godbus/dbus/v5"
	. "gopkg.in/check.v1"
)

func Test(t *testing.T) { TestingT(t) }

type fakeProxy struct {
	Interface  string
	MethodName string
	Args       []interface{}
	Result     *dbus.Call
}

func (f *fakeProxy) Call(method string, flags dbus.Flags, args ...interface{}) *dbus.Call {
	// store the called method and return Result
	i := strings.LastIndex(method, ".")
	f.Interface, f.MethodName = method[:i], method[i+1:]
	f.Args = args
	return f.Result
}

// returns a new error that can be used in the tests
func newDBusError() *dbus.Call {
	return &dbus.Call{
		Destination: "com.destination",
		Path: "/path",
		Method: "com.interface.method",
		Err: errors.New("testing error"),
	}
}

func newDBusReturn() *dbus.Call {
	return &dbus.Call{
		Destination: "com.destination",
		Path: "/path",
		Method: "com.interface.method",
	}
}
