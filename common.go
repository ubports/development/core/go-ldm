/*
 * Copyright 2014 Canonical Ltd.
 * Copyright 2024 Guido Berhoerster <guido+ubports@berhoerster.name>
 *
 * Authors:
 * Manuel de la Pena: manuel.delapena@canonical.com
 *
 * This file is part of lomiri-download-manager.
 *
 * lomiri-download-manager is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * lomiri-download-manager is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Package ldm provides a go interface to work with the lomiri download manager
package ldm

import (
	"github.com/godbus/dbus/v5"
)

// Progress provides how much progress has been performed in a download that was
// already started.
type Progress struct {
	Received uint64
	Total    uint64
}

// interface added to simplify testing
type proxy interface {
	Call(method string, flags dbus.Flags, args ...interface{}) *dbus.Call
}

var readCallArgs = func(c *dbus.Call, args ...interface{}) error {
	return c.Store(args...)
}

var readSignalArgs = func(s *dbus.Signal, args ...interface{}) error {
	return dbus.Store(s.Body, args...)
}

func getUint64Value(p proxy, method string) (value uint64, err error) {
	c := p.Call(method, 0)
	if c.Err != nil {
		err = c.Err
		return
	}

	err = readCallArgs(c, &value)
	return
}

func setUint64Value(p proxy, method string, value uint64) error {
	return p.Call(method, 0, value).Err
}

func setStringValue(p proxy, method, value string) error {
	return p.Call(method, 0, value).Err
}

func getBoolValue(p proxy, method string) (value bool, err error) {
	c := p.Call(method, 0)
	if c.Err != nil {
		err = c.Err
		return
	}

	err = readCallArgs(c, &value)
	return
}

func getMetadataMap(p proxy, method string) (value map[string]string, err error) {
	c := p.Call(method, 0)
	if c.Err != nil {
		err = c.Err
		return
	}

	err = readCallArgs(c, &value)
	return
}
